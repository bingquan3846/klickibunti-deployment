# Build Docker image after changes in Docker/Dockerfile

1. Build the docker image:

```
docker build -t registry.gitlab.com/mhuber84/klickibunti-deployment:latest .
```

2. Upload the docker image:

```
docker login registry.gitlab.com
docker push registry.gitlab.com/mhuber84/klickibunti-deployment
```
