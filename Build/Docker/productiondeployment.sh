#!/bin/sh

cd /build

eval $(ssh-agent -s)
echo "$SSH_PRIVATE_KEY" > /tmp/id_rsa
chmod 600 /tmp/id_rsa
ssh-add /tmp/id_rsa

git config --global user.email "gitlab@marco-huber.de"
git config --global user.name "Marco Huber"

cd /build/Build
SURF_WORKSPACE=".surfworkspace" \
Surf/surf.phar --configurationPath="Surf" -vvv deploy ProductionDeployment
